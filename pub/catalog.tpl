<%= (fill-header rc (format #f "Catalog: /~a/ - ~a - ~a" path1 board-title website-title)
                    (format #f "<h2>Catalog: /~a/ - ~a</h2>" path1 board-title)
                    style admin #:class "preview") %>

    <div class="catalog">
      <%= (build-catalog path1) %>
    </div>

<%= (fill-footer rc styles) %>
