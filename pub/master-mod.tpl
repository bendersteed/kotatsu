<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="noarchive">
    <meta name="description" content="Some message board">
    <@icon favicon.ico %>
    <link rel="stylesheet" href="/pub/css/<%= style %>.css ">
    <title><%= pagetitle %></title>
  </head>

  <body>
    <div class="header">
      <center>
        <h1>
          <%= website-title %><br>
          <span class="name">Admin - <%= (assoc-ref (car admin) "name") %></span>
        </h1>
        <%= message %>
        Admin links: [<a href="/panel">Admin Panel</a>] [<a href="/logoff">Logoff</a>]<br>
        [<a href="/index">HOME</a>] [<a href="/" target="_top">Frames</a>] [<a href="/about">About</a>] [<a href="/rules">Rules</a>] [<a href="/news">News</a>]
        <br>
      </center>
    </div>
    <hr>

    <div class="mid <%= class %> ">
    <br>
