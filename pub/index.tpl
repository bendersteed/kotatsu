<%= (fill-header rc website-title greeting style admin #:class "infopage") %>

    <center>
      <table><tr><td>

        <%= (string-join
             (map (lambda (board)
                   (format #f "<div style=\"margin:10px\">
                                 <h3><a href=\"/~a\">~a</a></h3>
                               </div>"
                              (car board) (assq-ref (cdr board) 'title)))
                  boards)
             "\n") %>

      </td></tr></table>
    </center>

    <br>

    [<a href="/news">View all news postings</a>]<br>
    <hr style="border-style:groove;border-width:0px 0px 2px 0px">

<%= (build-note-listing rc #:type "news" #:limit 3) %>

<%= (fill-footer rc styles) %>
