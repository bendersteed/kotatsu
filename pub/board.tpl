<%= (fill-header rc (format #f "/~a/ - ~a - ~a" path1 board-title website-title)
                    (format #f "<h2>/~a/ - ~a</h2>" path1 board-title)
                    style admin #:class "preview") %>

        <div class="news-box">
          <ul>
            <%= (build-note-listing rc #:template "pub/note-short.tpl" #:type "news" #:limit 3) %>
          </ul>
        </div>
    <center>
        <form enctype="multipart/form-data" method="post">
          <table class="postform" border="1">
            <tr>
              <td class="field">Options</td><td><input type="text" name="options" size="20"></td>
              <td class="field">Password</td><td><input type="password" name="password" size="20" placeholder="Use IP address if blank" value=<%= (get-password rc cookies) %>></td>
            </tr><tr>
              <td class="field">Subject</td><td colspan="3"><input type="text" name="subject" size="51"></td>
            </tr><tr>
              <td class="field">Name</td><td colspan="3"><input type="text" name="name" size="45"><input type="submit" name="submit" value="Post"></td>
            </tr><tr>
              <td class="field">Comment</td><td colspan="3"><textarea rows="5" cols="50" name="comment"></textarea></td>
            </tr><tr>
              <td class="field">File</td><td colspan="3"><input type="file" name="file"></td>
            </tr>
          </table>
        </form>
    </center>

    <br>

    <form enctype="multipart/form-data" action="/mod-posts" method="post">
      <%= (string-join (map (lambda (page)
                              (format #f "[<a href=\"/~a?page=~a\">~a</a>]" path1 (+ page 1) (+ page 1)))
                            (iota (assq-ref (assoc-ref boards path1)
                                            'pages)))
                       " ") %>
      [<a href="/<%= path1 %>/catalog">Catalog</a>]<br>
      <%= (format #f (if admin
                       "<span class=\"field\" style=\"padding:4px\">
                          <b>Mod Actions:</b> <input type=\"radio\" name=\"modaction\" value=\"del\">Delete
                                              <input type=\"radio\" name=\"modaction\" value=\"ban\">Ban
                                              <input type=\"radio\" name=\"modaction\" value=\"sticky\">Sticky
                                              <input type=\"submit\" value=\"submit\" value=\"Submit\">
                        </span><br>"
                       "")) %>
      <%= (build-threads path1 #f #:mode 'preview #:page (get-from-qstr rc "page")) %>
      <span style="float:right">Delete Post: <input type="submit" name="delete-button" value="Delete"></span>
    </form>

<%= (fill-footer rc styles) %>
